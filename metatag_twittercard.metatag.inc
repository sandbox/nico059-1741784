<?php

/**
 * @file
 * Metatag integration for the metatag_opengraph module.
 */

/**
 * Implements hook_metatag_config_default_alter().
 */
 /* TODO 
function metatag_twittercard_metatag_config_default_alter(array &$configs) {
  foreach ($configs as &$config) {
    switch ($config->instance) {
      case 'global':
        $config->config += array(
          'og:type' => array('value' => 'article'),
          'og:title' => array('value' => '[current-page:title]'),
          'og:site_name' => array('value' => '[site:name]'),
          'og:url' => array('value' => '[current-page:url]'),
        );
        break;
      case 'global:frontpage':
        $config->config += array(
          'og:type' => array('value' => 'website'),
          'og:title' => array('value' => '[site:name]'),
          'og:url' => array('value' => '[site:url]'),
        );
        break;
      case 'node':
        $config->config += array(
          'og:title' => array('value' => '[node:title]'),
          'og:description' => array('value' => '[node:summary]'),
        );
        break;
      case 'taxonomy_term':
        $config->config += array(
          'og:title' => array('value' => '[term:name]'),
          'og:description' => array('value' => '[term:description]'),
        );
        break;
      case 'user':
        $config->config += array(
          'og:type' => array('value' => 'profile'),
          'og:title' => array('value' => '[user:name]'),
        );
        if (variable_get('user_pictures')) {
          $config->config += array(
            'og:image' => array('value' => '[user:picture:url]'),
          );
        }

        break;
    }
  }
}
*/

/**
 * Implements hook_metatag_info().
 */
function metatag_twittercard_metatag_info() {

  $info['groups']['twitter-card'] = array(
    'label' => t('Twitter Card'),
    'description' => t('For now, use the open graph url / title / description / image as explain here https://dev.twitter.com/docs/cards'),
  );
  
  $info['tags']['twitter:card'] = array(
    'label' => t('Twitter card type'),
    'description' => t('- Minimum required for a valid Twitter Card -<br>
	Summary: The default card, which includes a title, description, thumbnail image, and Twitter account attribution.<br>
    Photo: A Tweet sized photo card.<br>
    Player: A Tweet sized video/audio/media player card.'),
    'group' => 'twitter-card',
    'class' => 'DrupalTextMetaTag',
    'form' => array(
      '#type' => 'select',
      '#options' => array('summary'=>t('Summary'),'photo'=>t('Photo - not yet implemented'),'player'=>t('Player - not yet implemented')),
      '#empty_option' => t('- None -'),
    ),
   'element' => array(
      '#theme' => 'metatag_opengraph',
    ),
  );  

   $info['tags']['twitter:site'] = array(
    'label' => t('Site owner'),
    'description' => t('@username for the website used in the card footer. Optional.'),
    'group' => 'twitter-card',
    'class' => 'DrupalTextMetaTag',
    'element' => array(
      '#theme' => 'metatag_opengraph',
    ),
  ); 
  
  $info['tags']['twitter:creator'] = array(
    'label' => t('Content creator'),
    'description' => t('@username for the content creator / author. Optional.'),
    'group' => 'twitter-card',
    'class' => 'DrupalTextMetaTag',
    'element' => array(
      '#theme' => 'metatag_opengraph',
    ),
  ); 
  
  return $info;
}
